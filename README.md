# FLORIDA

FLORIDA, or **F**lags **L**ike **O**rderly **R**esizing, **I**ntelligent **D**ecision **A**lways, is a simple bash script for batch flag editing and processing. The purpose of this script is to allow a user to specify a batch of flags for a desired region, and automate the time-intensive process of converting them into acceptable images for the [Extra-Flags-for-4chan](https://gitlab.com/flagtism/Extra-Flags-for-4chan) extension. This script also allows for the automatic generation of non-existing flags from existing Coats of Arms, emblems, symbols, logos, and so forth. The auto-generated flags can be built as simple as an icon upon a white field, or an icon upon a colored field with various designs.

Before:

[![Example-Flags-Full.gif](https://i.postimg.cc/8c9bvKBt/Example-Flags-Full.gif)](https://postimg.cc/rRGWX9Hr)

After: 
[![Example-Flags-Small.gif](https://i.postimg.cc/T2DW4RrH/Example-Flags-Small.gif)](https://postimg.cc/yW7dJCpF)

# Requirements

If you need help meeting any of these requirements, feel free to ask on /flag/ or open an issue on the repo.

*  Ability to run bash files. Linux or MacOS is recommended, but it's still easily doable on Windows.
*  [git](https://git-scm.com/) is required for (future) version control and automatic updates.
*  [Imagemagick](https://imagemagick.org/index.php) must be installed, or the script won't be able to interface with images.
*  [bc](https://www.gnu.org/software/bc/manual/html_mono/bc.html) is required if you wish to use the randomize feature when auto-generating flags.
*  [rename](https://linux.die.net/man/3/rename) is required for outputting flags with correct filename formatting.

# Get Started

Clone this directory to a safe location on your computer using the following terminal command: `git clone https://gitlab.com/Tampanon/FLORIDA`  
This only needs to be done once.

# Creating Flags

If you wish to create flags from scratch (e.g. utilizing a Coat of Arms, symbol, emblem, logo, or sillhouette), follow the instructions below:

1. Enter the cloned directory `cd FLORIDA`
2. Make the script executable `chmod +x FLORIDA.sh` 
3. Add the images you wish to use as flag centerpieces to the "FlagsToCreate" folder.
4. Run the script: `./FLORIDA.sh`
5. Check the "ConvertedFlags" folder upon completion for resulting flags.

# Resizing Flags

If you wish to resize pre-existing flags, follow the instructions below:

1. Enter the cloned directory `cd FLORIDA`
2. Make the script executable `chmod +x FLORIDA.sh`
3. Add the flags you wish to resize to the "FlagsToConvert" folder.
4. Run the script: `./FLORIDA.sh`
5. Check the "ConvertedFlags" folder upon completion for resulting flags.

# Troubleshooting

*"I see error messages while creating flags!"*
* They don't affect the script's functionality, and can be safely ignored. I'll fix it someday when I feel like it.

*"My flags disappeared!*
* Check the ConvertedFlags directory upon script completion.

# Future Plans

Recursively searching subdirectories within FlagsToCreate? Support for user-provided custom dimensions? You tell me
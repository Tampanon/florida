#!/bin/bash

cat << "Startup"
                                                                                                           
8 8888888888   8 8888         ,o888888o.     8 888888888o.    8 8888 8 888888888o.            .8.          
8 8888         8 8888      . 8888     `88.   8 8888    `88.   8 8888 8 8888    `^888.        .888.         
8 8888         8 8888     ,8 8888       `8b  8 8888     `88   8 8888 8 8888        `88.     :88888.        
8 8888         8 8888     88 8888        `8b 8 8888     ,88   8 8888 8 8888         `88    . `88888.       
8 888888888888 8 8888     88 8888         88 8 8888.   ,88'   8 8888 8 8888          88   .8. `88888.      
8 8888         8 8888     88 8888         88 8 888888888P'    8 8888 8 8888          88  .8`8. `88888.     
8 8888         8 8888     88 8888        ,8P 8 8888`8b        8 8888 8 8888         ,88 .8' `8. `88888.    
8 8888         8 8888     `8 8888       ,8P  8 8888 `8b.      8 8888 8 8888        ,88'.8'   `8. `88888.   
8 8888         8 8888      ` 8888     ,88'   8 8888   `8b.    8 8888 8 8888    ,o88P' .888888888. `88888.  
8 8888         8 888888888888 `8888888P'     8 8888     `88.  8 8888 8 888888888P'   .8'       `8. `88888. 

Startup

echo Welcome to FLORIDA!
sleep 3

echo Updating FLORIDA with changes made to upstream.
git fetch --all
git reset --hard origin/master
git pull

totalConvertedFlags=0

while true; do
	read -p "Have the flags you intend to convert already been created? (Y/N) " yn4
	case $yn4 in
		[Yy]* ) createdBool=$((createdBool=1)); break;;
		[Nn]* ) createdBool=$((createdBool=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

while true; do
	read -p "Are the flags you intend to convert all the same size? (Y/N) " yn
	case $yn in
		[Yy]* ) sameSizeBool=$((sameSizeBool=1)); break;;
		[Nn]* ) sameSizeBool=$((sameSizeBool=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

if [ "$sameSizeBool" == 1 ]
then
	while true; do
		read -p "Are they rectangle (16x11)? (Y/N) " yn2
		case $yn2 in
			[Yy]* ) rectangleBool=$((rectangleBool=1)); break;;
			[Nn]* ) rectangleBool=$((rectangleBool=0)); break;;
			* ) echo "Please enter yes (Y) or no (N).";;
		esac
	done
	
	if [ "$rectangleBool" == 0 ]
	then
		while true; do
			read -p "Are they square (11x11)? (Y/N) " yn3
			case $yn3 in
				[Yy]* ) squareBool=$((squareBool=1)); break;;
				[Nn]* ) squareBool=$((squareBool=0)); break;;
				* ) echo "Please enter yes (Y) or no (N).";;
			esac
		done
	fi

# Ask user whether or not they'd like to "flag-ify" created flags
while true; do
	read -p "Would you like the created flags to be colorful? (Y/N) " yn5
	case $yn5 in
		[Yy]* ) flagifyBool=$((flagifyBool=1)); break;;
		[Nn]* ) flagifyBool=$((flagifyBool=0)); break;;
		* ) echo "Please enter yes (Y) or no (N).";;
	esac
done

if [ "$flagifyBool" == 1 ]
then
	while true; do
		read -p "Which style of flag design would you like to use? (1-16, 17 for randomized) " yn6
		case $yn6 in
			1 ) colorStyle=$((colorStyle=1)); break;;
			2 ) colorStyle=$((colorStyle=2)); break;;
			3 ) colorStyle=$((colorStyle=3)); break;;
			4 ) colorStyle=$((colorStyle=4)); break;;
			5 ) colorStyle=$((colorStyle=5)); break;;
			6 ) colorStyle=$((colorStyle=6)); break;;
			7 ) colorStyle=$((colorStyle=7)); break;;
			8 ) colorStyle=$((colorStyle=8)); break;;
			9 ) colorStyle=$((colorStyle=9)); break;;
			10 ) colorStyle=$((colorStyle=10)); break;;
			11 ) colorStyle=$((colorStyle=11)); break;;
			12 ) colorStyle=$((colorStyle=12)); break;;
			13 ) colorStyle=$((colorStyle=13)); break;;
			14 ) colorStyle=$((colorStyle=14)); break;;
			15 ) colorStyle=$((colorStyle=15)); break;;
			16 ) colorStyle=$((colorStyle=16)); break;;
			17 ) colorStyle=$((colorStyle=17)); break;;
			* ) echo "Please enter an option between 1-17.";;
		esac
	done
fi

elif [ "$sameSizeBool" == 0 ]
then
	echo "Unfortunately, this script only supports creating flags of the same size. For flags with unique dimensions, you will have to create them manually."
	exit
fi

if [ "$rectangleBool" == 1 ]
then
	size=16x11
	fieldSize=1024x683
	gloss="Gloss Template (Generic).png"

elif [ "$rectangleBool" == 0 ]
then
	size=11x11
	fieldSize=1024x1024
	gloss="Gloss Template (Square).png"
fi

echo Gloss being used is $gloss
echo Getting ready to create/convert flags of size $size! Please wait...
sleep 5

# Logic to create new flags from Coats of Arms if not already created
if [ "$createdBool" == 0 ]
then	
	if [ "$colorStyle" == 17 ]
	then
		randomBool=1
	fi

	# For each file in FlagsToCreate
	for file in FlagsToCreate/*
	do
		# Create 1920x1080 white field
		if test -f "$file"
		then
			# For colorful, flag-ified flags
			if [ "$flagifyBool" == 1 ]
			then
				mapfile -t colorArray < <(convert $file -format %c -colorspace LAB -colors 2 histogram:info:- | sort -n -r | cut -c 27- | cut -f1 -d" ")
				color1=${colorArray[0]}
				color2=${colorArray[1]}
				fieldColor=$color1
				
				if [ "$randomBool" == 1 ]
				then
					colorStyle=`echo $RANDOM % 16 + 1 | bc`
				fi

				# Dominant color background
				if [ "$colorStyle" == 1 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color1" -draw "color 1,1 floodfill" "$file".png
				fi

				# Second-dominant color background
				if [ "$colorStyle" == 2 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color2" -draw "color 1,1 floodfill" "$file".png
				fi

				# Horizontal borders, dominant color
				if [ "$colorStyle" == 3 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color1" -draw "color 1,1 floodfill" "$file".png
					convert "$file".png -shave 0x171 -bordercolor $color2 -border 0x171 "$file".png
				fi

				# Horizontal borders, second-dominant color
				if [ "$colorStyle" == 4 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color2" -draw "color 1,1 floodfill" "$file".png
					convert "$file".png -shave 0x171 -bordercolor $color1 -border 0x171 "$file".png
				fi

				# Vertical borders, dominant color
				if [ "$colorStyle" == 5 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color1" -draw "color 1,1 floodfill" "$file".png
					convert "$file".png -shave 256x0 -bordercolor $color2 -border 256x0 "$file".png
				fi

				# Vertical borders, second-dominant color
				if [ "$colorStyle" == 6 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color2" -draw "color 1,1 floodfill" "$file".png
					convert "$file".png -shave 256x0 -bordercolor $color1 -border 256x0 "$file".png
				fi

				# Diagonal LL->UR, dominant color above
				if [ "$colorStyle" == 7 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color1" -draw "line 1,683 1024,1" "$file".png
					#composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill "$color1" -draw "color 1,1 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 1016,600 floodfill" "$file".png
				fi

				# Diagonal LL->UR, dominant color below
				if [ "$colorStyle" == 8 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke $color2 -draw "line 1,683 1024,1" "$file".png
					convert "$file".png -fill "$color2" -draw "color 1,1 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 1016,600 floodfill" "$file".png
				fi

				# Diagonal UL->LR, dominant color above
				if [ "$colorStyle" == 9 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color1" -draw "line 0,0 1024,683" "$file".png
					convert "$file".png -fill "$color1" -draw "color 1016,50 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 50,600 floodfill" "$file".png
				fi

                # Diagonal UL->LR, dominant color below (not working for some reason?)
				if [ "$colorStyle" == 10 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color2" -draw "line 0,0 1024,683" "$file".png
					convert "$file".png -fill "$color2" -draw "color 1016,50 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 50,600 floodfill" "$file".png
				fi

				# Criss-cross, dominant color left/right
				if [ "$colorStyle" == 11 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color1" -draw "line 0,0 1024,683" "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color1" -draw "line 1,683 1024,1" "$file".png
					convert "$file".png -fill "$color1" -draw "color 90,336 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 948,336 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 522,36 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 526,662 floodfill" "$file".png
				fi
				
				# Criss-cross, dominant color top/bottom
				if [ "$colorStyle" == 12 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color2" -draw "line 0,0 1024,683" "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color2" -draw "line 1,683 1024,1" "$file".png
					convert "$file".png -fill "$color2" -draw "color 90,336 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 948,336 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 552,36 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 526,662 floodfill" "$file".png
				fi

				# Horizontal bi-color, dominant color top
				if [ "$colorStyle" == 13 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color1" -draw "line 0,342 1024,342" "$file".png
					convert "$file".png -fill "$color1" -draw "color 108,90 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 102,564 floodfill" "$file".png
				fi

				# Horizontal bi-color, dominant color bottom
				if [ "$colorStyle" == 14 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color2" -draw "line 0,342 1024,342" "$file".png
					convert "$file".png -fill "$color2" -draw "color 108,90 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 102,564 floodfill" "$file".png
				fi

				# Vertical bi-color, dominant color left
				if [ "$colorStyle" == 15 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color1" -draw "line 512,0 512,683" "$file".png
					convert "$file".png -fill "$color1" -draw "color 82,346 floodfill" "$file".png
					convert "$file".png -fill "$color2" -draw "color 968,346 floodfill" "$file".png
				fi

				# Vertical bi-color, dominant color right
				if [ "$colorStyle" == 16 ]
				then
					convert -size $fieldSize xc:white temp.png
					composite -gravity center "$file" temp.png "$file".png
					convert "$file".png -fill none -strokewidth 1 -stroke "$color2" -draw "line 512,0 512,683" "$file".png
					convert "$file".png -fill "$color2" -draw "color 82,346 floodfill" "$file".png
					convert "$file".png -fill "$color1" -draw "color 968,346 floodfill" "$file".png
				fi
			fi

			# For traditional, CoA on white background flags
			if [ "$flagifyBool" == 0 ]
			then
				convert -size $fieldSize xc:white temp.png
				composite -gravity center "$file" temp.png "$file".png
			fi
			echo "$file" flag created!
			totalCreatedFlags=$((totalCreatedFlags+1));
		fi
	done

	# Redundant loop to rename to .pngs (currently giving some "annoying" naming issues, but works) 
	for file in FlagsToCreate/*
	do
		if test -f "$file"
		then
			mv -- "$file" "${file%.*}.png" 2>/dev/null
		fi
		mv "$file" FlagsToConvert/
	done
fi

# Resize flags to rectangle/square dimensions, and apply gloss
for file in FlagsToConvert/*
do
    if test -f "$file"
    then
        if [[ "$file" != "output.png" || "$file" != "gloss.png" ]]
        then
            echo Converting "$file"...
            convert "$file" -resize $size -quality 100 "$file"
            echo Compositing "$file"...
            composite -gravity center "$gloss" "$file" "$file"
            echo File converted!
            totalConvertedFlags=$((totalConvertedFlags+1));
        fi
    fi
done

# Redundant loop because I'm too lazy to implement a workaround for filename interfacing
echo Converting all files to .png...
for file in FlagsToConvert/*
do
    if test -f "$file"
    then
	rename 's/.jpg//' *.png
        mv -- "$file" "${file%.*}.png" 2>/dev/null
    fi
    mv "$file" ConvertedFlags/
done

for file in FlagsToCreate/*
do
	rm "$file"
done


# Remove non.png extensions from original filename (for some reason couldn't get this to work inside of a loop?)
echo Removing redundant extensions...
cd ConvertedFlags
		rename 's/.jpg//' *.png
		rename 's/.gif//' *.png
		rename 's/.jpeg//' *.png
cd ..

echo =======================================================================
echo $totalCreatedFlags flags have been created.
echo $totalConvertedFlags flags have been converted.
echo Please double-check before repo submission for quality-assurance.
echo And remember, Flags Like Orderly Resizing, Intelligent Decision Always!
echo =======================================================================
